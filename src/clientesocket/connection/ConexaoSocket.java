/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientesocket.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class ConexaoSocket {

    public String conectarSocket(String ip, int porta) {

        //Definindo objeto do tipo socket
        Socket objConexao;
        //Definindo a váriavel do tipo String
        String mensagem = null;
        try {
            //Instanciando o objemto do tipo Socket para receber a mensagem do servidor
            objConexao = new Socket(ip, porta);
            //Instanciando o objeto responsável por receber a mensagem via Socket do servidor
            ObjectInputStream entrada = new ObjectInputStream(objConexao.getInputStream());
            //Atribuindo a mensagem recebida do servidor para uma váriavel do tipo String
            mensagem = entrada.readUTF();
            //Finalizando as instancias dos objetos da comunicação cliente socket
            entrada.close();
            objConexao.close();
        } catch (Exception ex) {
            // Está mensagem será exibida caso ocorrer algum erro com a inicialização da comunicação socket com o servidor
            JOptionPane.showMessageDialog(null, "Erro ao conectar com o servidor socket. Porta Padrão: 1050", "Erro", 2);
        }
        return mensagem;
    }
}
