package clientesocket.file;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import ldsutils.TXTPersist;


public class Arquivo {

    //Método reponsável por fazer a leitura dos dados escritas no arquivo
    public List<String> lerArquivo() {
        List<String> linhaList = new ArrayList<String>();
        try {
            //Recebendo os dados armazenados no arquivo
            linhaList = TXTPersist.readFromFile("C:\\log\\log.txt");
            //Exibe uma mensagem para usuário que a mensagem foi salva no arquivo
            JOptionPane.showMessageDialog(null, "Mensagem salva no arquivo!", "Sucesso", 2);
        } catch (Exception ex) {
            //Exibe uma mensagem para usuário caso ocorrer erro na leitura do arquivo
            JOptionPane.showMessageDialog(null, "Erro ao ler arquivo", "Erro", 2);
        }
        return linhaList;
    }
    
    
    //Método reponsável por escrever e gravar as mensagens escritas no arquivo
    public void gravarArquivo(List<String> linhaList){
        try {
            //Método que por escrever e gravar os dados no arquivo
            TXTPersist.saveToFile(linhaList, "C:\\log\\log.txt");
        } catch (Exception ex) {
            //Exibe uma mensagem para usuário caso ocorrer erro na escrita ou gravação do arquivo
            JOptionPane.showMessageDialog(null, "Erro ao gravar arquivo", "Erro", 2);
        }
    }

}
