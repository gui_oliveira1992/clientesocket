package clientesocket;

import clientesocket.swing.FormTelaPrincipal;

public class ClienteSocket {

    public static void main(String[] args) {
        //Instanciando o objeto de interface com usuaário
        FormTelaPrincipal formTelaPrincipal = new FormTelaPrincipal();
        //Chamada do método que abre a interface gráfica ao usuário da aplicação
        formTelaPrincipal.show(true);
    }

}
